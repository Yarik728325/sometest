import React from "react";
import { NavLink } from "react-router-dom";
import Button from "../Button";
import "./style.scss";
import DropDown from "../DropDown";

const Navigation = ({isDroped, setDroped})=>{
  return(
    <div className="nav_wrapper">
      <h2 className="logo">Express</h2>
      <nav className={isDroped?"opened__nav":''}>
          <ul id="navigation__ul" className={isDroped?"droped":""} >
          <div 
          className="burger"
          onClick={()=>{
            setDroped(prevState=>!prevState)
          }}
          >
            <span></span><span></span><span></span>
          </div>
            <li><NavLink to="/"> Home</NavLink> </li>
            <li><NavLink to="/about"> About Us </NavLink> </li>
            <DropDown check = {isDroped} value={' Our services'} responsive={isDroped} />
            <li><NavLink to ="/contact" > Contact us</NavLink>  </li>
          </ul>
        </nav>
        <Button ownClass={"header"} value={"Call us"}  />
    </div>
  )
}

export default Navigation;
import React from "react";
import './style.scss';

const Button = ({ownClass, value}) =>{
  return(
    <div className={`call_wrapper ${ownClass || ''}`}>
      <button  className={`call_us_info ${ownClass || ''}`}>{value}</button>
    </div>
  )
}

export default Button;
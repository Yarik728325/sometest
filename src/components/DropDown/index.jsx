import React,{ useState } from "react";
import { Link } from "react-router-dom";
import './style.scss';

const DropDown = ({value})=>{
  const [ isDroped, setDroper] = useState(false);
  return(
    <li className="dropdown">
      <div 
      className={isDroped?"active":"dropdown__subtitle"}
      id="dropdown_id_sub"
      onClick={
        ()=>{
          setDroper(prevState=>!prevState)
        }
      }>{value}</div>
        <div  className='dropdown__wrapped' >
          <ul 
          id="drop_down__nav" 
          className={isDroped?"active":"ownClass"}  >
            <li><Link to="/" >Lorem ipsum</Link></li>
            <li><Link to="/" >Lorem ipsum</Link></li>
            <li><Link to="/" >Lorem ipsum</Link></li>
          </ul>
        </div>
    </li>
  )
}

export default DropDown;
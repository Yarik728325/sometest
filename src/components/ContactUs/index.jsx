import React from "react";
import './style.scss';
import { TextField } from '@material-ui/core';
import Button from "../Button";

const ContactUs = ()=>{
  return(
   <section className="contact_us">
     <div className="contact_us__blur"></div>
      <div className="contact_us__div" id="contact_us__resp">
      <h2 className="contact_us__title">Contact <span>Us</span></h2>
      <form action="" >
        <div className="wrapper">
          <TextField  id="outlined-basic" label="First Name" variant="outlined" />
          <TextField id="outlined-basic" label="Last Name" variant="outlined" />
          <TextField id="outlined-basic" label="Enter email" variant="outlined" />
          <TextField  id="outlined-basic" label="First Name" variant="outlined" />
          <TextField
            id="filled-textarea"
            placeholder="Tell Us What Happened"
            multiline
            variant="filled"
            className="last"
          />
        </div>
        <div className="contact_us__button">
          <Button value="Send" ownClass="contact" />
        </div>
      </form>
    </div>
   </section>
  )
}

export default ContactUs;
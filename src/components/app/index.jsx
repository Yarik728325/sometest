import React,  { useState }from "react"; 
import Routing from "../../routes";
import Navigation from "../Navigation";
import './style.scss';
import Footer from "../Footer";

const App =  ()=>{
  const [isDroped, setDroped] = useState(false); 
  return (
    <>
     <header className={isDroped?"changeZ":''}>
        <Navigation 
        isDroped={isDroped}
        setDroped={setDroped}
         />
      </header>
      <main className="container">
        <div className="left_svg"></div>
        <div className="right_svg"></div>
       <Routing/>
      </main>
      <Footer/>
    </>
  )
}

export default App;
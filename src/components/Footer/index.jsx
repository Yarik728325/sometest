import React  from "react";
import './style.scss'

const Footer = ()=>{
  return(
    <footer className="footer">
      <div className="footer__wrapper">
        <div className="item">
          <h2>Express</h2>
          <div className="footer__items">All Rights Reserved to info.com</div>
          <div className="footer__items">© DDD – Web Marketing</div>
          <div className="footer__items">Privacy Policy</div>
          <div className="footer__items"></div>
        </div>
        <div className="item">
          <h2>CONTACT US</h2>
          <div className="footer__items">
            <div className="footer__items_flex">
              <div className="left">UK</div>
              <div className="right">
                <span>+44-2020202020</span>
                <span>+44-2020202020</span>
              </div>
            </div>
          </div>
          <div className="footer__items">
            <div className="footer__items_flex">
              <div className="left">AUS</div>
              <div className="right">
                <span>+44-2020202020</span>
              </div>
            </div>
          </div>
          <div className="footer__items">
            <div className="footer__items_flex">
              <div className="left">AUT</div>
              <div className="right">
                <span>+44-2020202020</span>
              </div>
            </div>
          </div>
        </div>
        <div className="item">
          <h2>ADDRESS</h2>
          <div className="footer__items">Lorem ipsum <br /> set amet</div>
         <h2>OUR OFFICE HOURS</h2>
          <div className="footer__items">Mon-Fr: 8:00-18:00 GMT <br /> Sat/Su: Closed</div>
        </div>
        <div className="item">
          <h2>Email US</h2>
          <div className="footer__items">info@info.com</div>
        </div>
      </div>
    </footer>
  )
}

export default Footer;
import React from "react";
import './style.scss'
import Button from "../Button";

const AboutUs = ()=>{
    return( 
      <section className="about_us">
        <h1 className="about_us__title" >Lorem ipsum set ammet test-test</h1>
        <h2 className="about_us__subtitle">LET US HELP YOU GET IT ALL BACK!</h2>
        <div className="about_us__img" />
        <div className="about_us__wrapper">
          <div className="info">
          We currently take cases that are $10,000 US and up
          </div>
          <Button ownClass={'about'} value={'Get a free consultation'}/>
        </div>
      </section>
    )
}

export default AboutUs;
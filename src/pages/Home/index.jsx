import React from "react";
import AboutUs from "../../components/AboutUs";
import ContactUs from "../../components/ContactUs";

const Home = ()=>{
  
  return (
    <>
      <AboutUs/>
      <ContactUs/>
    </>
  )
}

export default Home;